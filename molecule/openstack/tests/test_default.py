import os

import testinfra.utils.ansible_runner
import lxml.html
from io import StringIO

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_openscap_compliance(host):

    host.run(
        "oscap xccdf eval "
        "--fetch-remote-resources "
        "--datastream-id "
        "scap_org.open-scap_datastream_from_xccdf_ssg-rhel7-xccdf-1.2.xml "
        "--xccdf-id scap_org.open-scap_cref_ssg-rhel7-xccdf-1.2.xml "
        "--profile "
        "xccdf_org.dreamerlabs.content_profile_stig-centos7-disa_customized "
        "--tailoring-file /root/ssg-centos7-ds-tailoring.xml "
        "--results /tmp/xccdf-results.xml "
        "--results-arf arf.xml "
        "--report /tmp/centos7-disa-stig-report_after.html "
        "--oval-results "
        "/usr/share/xml/scap/ssg/content/ssg-centos7-ds.xml"
    )

    html = host.file("/tmp/centos7-disa-stig-report_after.html").content_string
    html = lxml.html.parse(StringIO(html))
    oscap_results_preparsed = html.xpath(
        '//*[@id="compliance-and-scoring"]/div[2]/div[2]/text()'
    )
    oscap_results = int(oscap_results_preparsed[0].split(" ")[0])

    assert oscap_results <= 1
